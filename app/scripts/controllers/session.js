'use strict';

function sessionCtrl($scope, $state, focus) {

  $scope.user = {};
  $scope.signin = function () {
    $state.go('user.signin');
  };

  $scope.submit = function () {
    if (!$scope.user.username || $scope.user.username.trim() === '') {
      // toastr.error('Bạn vui lòng nhập tên đăng nhập.', 'Lỗi');
      focus('username');
      return;
  }

  // Password?
  if (!$scope.user.password || $scope.user.password.trim() == '') {
      //toastr.error('Bạn vui lòng nhập mật khẩu.', 'Lỗi');
      focus('password');
      return;
  }
    $state.go('app.dashboard');
  };
}

angular
  .module('urbanApp')
  .controller('sessionCtrl', ['$scope', '$state','focus', sessionCtrl]);
